﻿from django.urls import path
from image import settings

from main import views
from django.conf.urls.static import static

app_name = 'main'

urlpatterns = [
    path('', views.indexpage, name='index'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)