from django.db import models

# Create your models here.
class UploadFiles(models.Model):
    file = models.ImageField(upload_to='uploads_model')
    
    class Meta():
        db_table = 'image'