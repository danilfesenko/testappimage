import io
import sys
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from main.forms import ImageForm
from main.models import UploadFiles

from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image

# Create your views here.

def indexpage(request):
    if request.method == "POST":
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            image = form.cleaned_data['image']
            
            # image = request.FILES['image']
            img = Image.open(image)
            
            # Добавляем рамку
            border = Image.new('RGB', (img.width + 20, img.height + 20), 'white')
            border.paste(img, (10, 10))
            
             # Добавляем смайлик
            smiley = Image.open('static/smiley.png')
            border.paste(smiley, (0, 0), smiley)
            
            # Сохраняем отредактированное изображение
            output = io.BytesIO()
            border.save(output, format='JPEG', quality=85)
            output.seek(0)
            border = InMemoryUploadedFile(output, 'ImageField','123','image/jpeg',sys.getsizeof(output), None)
        
            image = UploadFiles(file=border)
            image.save()
            
            return HttpResponseRedirect(reverse('main:index'))
    else:
        form = ImageForm()
    
    image = UploadFiles.objects.all().order_by('-pk')
    return render(request, 'index.html', {'form': form, 'image': image})